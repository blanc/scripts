#!/bin/sh
# -*- coding: utf-8 -*-

# Copyright (C) 2009 Michel Blockelet <blockelet@crans.org>

# This file is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.


### Plugin munin de comptabilisation des posts de moins de 24h


## Configuration
if [ "$1" = "config" ]; then
    echo 'host_name news.crans.org'
    echo 'graph_title Posts de moins de 24h'
    echo "graph_args --base 1000 --lower-limit 0"
    echo "graph_vlabel posts"
    echo "graph_category News"
    echo "graph_info Affiche le nombre de posts de moins de 24h."
    echo "posts.label posts"
    exit 0
fi

## Fichier(s) temporaire(s)
TMPFILE="/tmp/munin_innd_last"

## Récupération des logs
# Si news.daily est en cours d'exécution, on réutilise le fichier précédent
if ! ( [ -f /var/run/news/LOCK.news.daily ] && [ -f $TMPFILE ] )
then
  zcat -f /var/log/news/OLD/news.notice.3.gz /var/log/news/OLD/news.notice.2.gz /var/log/news/OLD/news.notice.1.gz /var/log/news/news.notice | grep "post ok" > $TMPFILE 2> /dev/null
fi

## Récupération des 24 dernières heures
CURDATE=`date +%s`
DATE=0
while [ $(($CURDATE - $DATE)) -gt 86400 ]
do
    tail -n +2 $TMPFILE > ${TMPFILE}2
    cp ${TMPFILE}2 $TMPFILE
    DATESTR=`head -n 1 $TMPFILE | awk '{print $1,$2,$3}'`
    DATE=`date -d "$DATESTR" +%s`
done

## Comptage
echo -n "posts.value "
cat $TMPFILE | wc -l

rm -f ${TMPFILE}2 &> /dev/null
