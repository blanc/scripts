#!/usr/bin/env python
# -*- coding: utf-8 -*-

import string, re, sys, os

# # # # # # # # # # # # # # #
# Envoyer des e-mails       #
# # # # # # # # # # # # # # #
SENDMAIL = "/usr/sbin/sendmail"  # sendmail location

def sendmail(text):
    p = os.popen("%s -t" % SENDMAIL, "w")
    p.write(text)
    sts = p.close()
    if sts != None:
        print "Sendmail exit status :", sts

