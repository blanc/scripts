# -*- coding: utf-8 -*-
class FichierInvalide(Exception):
    """
    Exception renvoyée lorsqu'un fichier ne passe pas.
    utilisée avec deux arguments : une chaîne décrivant l'erreur et une chaîne avec le nom du fichier
    """
    def __str__(self):
        """
        Description de l'erreur.
        """
        return self.args[0]
    def file(self):
        """
        Nom du fichier qui pose problème
        """
        try:
            return self.args[1]
        except:
            return "n/a"

class SoldeInsuffisant(Exception):
    """
    Solde insuffisant pour l'impression demandée
    """
    pass
class PrintError(Exception):
    """
    Erreur lors de l'impression
    """
    pass
class SettingsError(Exception):
    """
    Erreur de paramètres.
    """
    pass
# ######################################################## #
#                       ERREURS                            #
# ######################################################## #
#
