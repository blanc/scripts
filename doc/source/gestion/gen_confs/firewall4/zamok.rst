
zamok -- Le pare-feu ipv4 de zamok
==================================

.. automodule:: gestion.gen_confs.firewall4.zamok
   :members:
   :special-members:
