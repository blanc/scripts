.. Les scripts du Cr@ns documentation master file, created by
   sphinx-quickstart on Wed Mar 20 18:39:09 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation sphinx des scripts du Cr@ns
=========================================

Table des matières :

.. toctree::
   :maxdepth: 5
   
   gestion/index
   impression/index

   others

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

