#!/usr/bin/env python
# -*- encoding: utf-8 -*-

def decode_dammit(s):
    """Renvoie un unicode. Que ``s`` soit un unicode, un str en UTF-8 ou un str en ISO-8859-1.
       Dans les autres cas, plante en affichant la chaîne incriminée."""
    if not isinstance(s, basestring):
        raise ValueError("Appeler avec str ou unicode.")
    if isinstance(s, unicode):
        return s
    else:
        try:
            return s.decode("utf-8")
        except UnicodeDecodeError as error:
            try:
                return s.decode("iso-8859-1")
            except UnicodeDecodeError as error2:
                raise UnicodeDecodeError("%s: %r" % (error2, v))
