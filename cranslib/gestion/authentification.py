#!/usr/bin/env python
# -*- coding: utf-8 -*-
# #############################################################
#                                            ..                
#                       ....  ............   ........          
#                     .     .......   .            ....  ..    
#                   .  ... ..   ..   ..    ..   ..... .  ..    
#                   .. .. ....@@@.  ..  .       ........  .    
#              ..  .  .. ..@.@@..@@.  .@@@@@@@   @@@@@@. ....  
#         .@@@@. .@@@@. .@@@@..@@.@@..@@@..@@@..@@@@.... ....  
#       @@@@... .@@@.. @@ @@  .@..@@..@@...@@@.  .@@@@@.    .. 
#     .@@@..  . @@@.   @@.@@..@@.@@..@@@   @@ .@@@@@@..  ..... 
#    ...@@@.... @@@    .@@.......... ........ .....        ..  
#   . ..@@@@.. .         .@@@@.   .. .......  . .............  
#  .   ..   ....           ..     .. . ... ....                
# .    .       ....   ............. .. ...                     
# ..  ..  ...   ........ ...      ...                          
#  ................................                            
#                                                              
# #############################################################
# authentification.py
#
#     Fonction de login et petite classe utilisateur 
#
# Auteur: Grégoire Détrez <gdetrez@crans.org>
# Copyright (c) 2008 by www.crans.org
# #############################################################
import ldap
#import cransldap
import models
import fields
LDAP_SERVER = "ldap.adm.crans.org"

# Petite classe Avec les infos de base de l'utilisateur
class User(models.Model):
    prenom = fields.stringField( "Prénom" )
    nom = fields.stringField( "Nom" )
    mail = fields.stringField( "Couriel" )
    uid = fields.loginField( "Identifiant" )
    aid = fields.intField( "aid" )
    droits = fields.listField( "Droits", fields.stringField(None) )

    def is_nounou(self):
        return u"Nounou" in self.droits

class AuthentificationFailed(Exception):
    def __str__(self):
        return "Oups, auth failed"

def login( id, password ):
    """ returns a User object """
    con = ldap.open( LDAP_SERVER )
    try:
        user_dn = get_dn_from_id( id, con )
    except ValueError:
        raise AuthentificationFailed
    try:
        con.bind_s(user_dn, password)
    except ldap.INVALID_CREDENTIALS, e:
        raise AuthentificationFailed
    data = con.search_s(user_dn, ldap.SCOPE_SUBTREE)
    con.unbind()
    data = data[0][1]
    return User(data)

def get_dn_from_id( id, ldap_connexion=None ):
    if ldap_connexion == None:
        ldap_connexion = ldap.open( LDAP_SERVER )
    try:
        aid = int( id )
        user_dn = "aid=%d,ou=data,dc=crans,dc=org" % aid
        return user_dn
    except ValueError:
        data = ldap_connexion.search_s("ou=data,dc=crans,dc=org", 
                                       ldap.SCOPE_SUBTREE, 
                                       "uid=%s" % id)
        if len(data) != 1:
            raise ValueError, "Id invalid"
        else: return data[0][0]

