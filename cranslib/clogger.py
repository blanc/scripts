#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Basic logger, storing data in /var/log/clogger/*.log
# Author    : Pierre-Elliott Bécue <becue@crans.org>
# License   : GPLv3
# Date      : 27/04/2014

import logging

class CLogger(logging.Logger):
    """
    Crans logger
    """

    def __init__(self, loggerName, service, level, debug=False):
        """
        Initializes logger. The debug variable is useful to have a print to stdout (when debugging)
        """
        super(CLogger, self).__init__(loggerName)

        # Creates FileHandler
        self.fh = logging.FileHandler("/var/log/clogger/%s.log" % (loggerName,))

        # Catches appropriate level in logging.
        self.fhlevel = getattr(logging, level.upper(), logging.INFO)
        self.fh.setLevel(self.fhlevel)

        # Creates formatter
        self.formatter = logging.Formatter('%%(asctime)s - %%(name)s - %(service)s - %%(levelname)s - %%(message)s' % {'service': service})

        # Adds formatter to FileHandler
        self.fh.setFormatter(self.formatter)

        if debug:
            self.sh = logging.StreamHandler()
            self.shlevel = logging.DEBUG
            self.sh.setLevel(self.shlevel)
            self.sh.setFormatter(self.formatter)
            self.addHandler(self.sh)

        # Adds FileHandler to Handlers
        self.addHandler(self.fh)
