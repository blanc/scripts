#! /bin/sh

SAP_TXT="/tmp/chaines_recup_sap.txt"

DB_TV="/etc/bind/generated/db.tv.crans.org"
DB_239="/etc/bind/generated/db.239.in-addr.arpa"

cat > $DB_TV <<EOF
\$ORIGIN tv.crans.org.
\$TTL 86400
@       IN      SOA mdr.crans.org. root.crans.org. (
$(date +%s) ; numero de serie
21600 ; refresh (s)
3600 ; retry (s)
1209600 ; expire (s)
86400 ; TTL (s)
)

; DNS de la zone par ordre de priorit�
@       IN      NS mdr.crans.org.
@       IN      NS charybde.crans.org.
@       IN      NS freebox.crans.org.

@       IN      A  138.231.136.243

EOF

sed -r 's/(fra|eng|ger|ara|rom|rad|rus|ned|ita|vo|x-ero|autres|sport|TNT[0-9]{2})//g' $SAP_TXT | sed "s/[ _()'\"]//g" | tr A-Z a-z | awk -F ':' '{ gsub("\\.","",$1 ); print $1 "\tIN\tA\t" $NF }' >> $DB_TV

cat > $DB_239 <<EOF
\$ORIGIN 239.in-addr.arpa.
\$TTL 86400
@       IN      SOA mdr.crans.org. root.crans.org. (
$(date +%s) ; numero de serie
21600 ; refresh (s)
3600 ; retry (s)
1209600 ; expire (s)
86400 ; TTL (s)
)

; DNS de la zone par ordre de priorit�
@       IN      NS mdr.crans.org.
@       IN      NS charybde.crans.org.
@       IN      NS freebox.crans.org.

EOF

sed -r 's/(fra|eng|ger|ara|rom|rad|rus|ned|ita|vo|x-ero|autres|sport|TNT[0-9]{2})//g' \
      $SAP_TXT | sed "s/[ _()'\"]//g" | tr A-Z a-z \
      | awk -F ':' '{gsub("\\.","",$1 ); split($NF,ip,"."); print ip[4]"."ip[3]"."ip[2] "\tIN\tPTR\t" $1 ".tv.crans.org."}' >> $DB_239

/etc/init.d/bind9 reload 1> /dev/null
