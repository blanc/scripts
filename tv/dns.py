#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.append('/usr/scripts')
import re
from unidecode import unidecode
from gestion.config import dns


def is_ascii(s):
    return all(ord(c) < 128 for c in s)


def _sanitize(nom):
    nom=re.sub('TNT([0-9]*) ','',nom) # on enlève les TNT## des noms
    nom=nom.replace('TNT%2lcn ','') # on enlève les TNT## des noms
    nom=nom.replace('&','and') # on enlève les TNT## des noms
    nom=re.sub('!|\[|\]|\'','', nom) # on enlève les ! [ ] ...
    nom=re.sub(' +([^ ])','-\g<1>',nom) # on remplaces les espaces intérieur par un tiret
    nom=re.sub('[ .():,"\'+<>]','',nom) # on enlève tous les caractères illégaux
    return nom.lower()

def _fqdn(bool):
    if bool:
        return ".%s" % dns.zone_tv
    else:
        return ""

def _check_unicode(nom):
    if not isinstance(nom, unicode):
        raise UnicodeError("%r should by an unicode string" % nom)

def punycode(nom, fqdn=False):
    _check_unicode(nom)
    if is_ascii(nom):
        return None
    else:
        return "xn--%s%s" % (_sanitize(nom).encode('punycode'), _fqdn(fqdn))

def ascii(nom, fqdn=False):
    _check_unicode(nom)
    return "%s%s" % (unidecode(_sanitize(nom)), _fqdn(fqdn))

def idn(nom, fqdn=False, charset='utf-8'):
    _check_unicode(nom)
    if charset:
        return "%s%s" % (_sanitize(nom).encode(charset), _fqdn(fqdn))
    else:
        return u"%s%s" % (_sanitize(nom), _fqdn(fqdn))
