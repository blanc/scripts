#!/usr/bin/python
# -*- coding: utf8 -*-
import urlparse
import urllib
from config import *

i=0
for group in multicast.keys():
	for (title, (name,dst,port,sources)) in multicast[group].items():
		p=urlparse.urlparse(sources[0])
                if ':' in p.netloc:
			host=p.netloc.split(':')[0]
			port=p.netloc.split(':')[1]
		else:
			host=p.netloc
			port = 80
		print """
<relay>
        <server>%s</server>
        <port>%s</port>
        <mount>%s</mount>
        <local-mount>/%s</local-mount>
        <relay-shoutcast-metadata>1</relay-shoutcast-metadata>
        <on-demand>1</on-demand>
</relay>""" % (host, port, p.path, name)
