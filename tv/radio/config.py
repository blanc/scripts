# -*- coding: utf8 -*-
import os, random, collections

def dir(path):
	l=collections.deque(["%s%s" % (path,i) for i in os.listdir(path)])
	l.rotate(random.randrange(0,len(l)))
	return l
multicast={
'Radio': {
	'Armitunes':			('armitunes','239.231.140.162','1234',['http://198.27.80.17:8000/','http://95.31.11.136:9010/','http://95.31.3.225:9010/']),
	'Radio Classique':		('classique','239.231.140.163','1234',['http://broadcast.infomaniak.net:80/radioclassique-high.mp3']),
	'France Inter':			('inter','239.231.140.164','1234',['http://mp3.live.tv-radio.com/franceinter/all/franceinterhautdebit.mp3']),
	'France Info':			('info','239.231.140.165','1234',['http://mp3.live.tv-radio.com/franceinfo/all/franceinfo-32k.mp3']),
#	'Webradio Chibre':		('chibre','239.231.140.166','1234',['http://webradio.crans.org:8000/chibre.mp3']),
#	'Webradio Clubbing':		('clubbing','239.231.140.167','1234',['http://webradio.crans.org:8000/clubbing.mp3']),
#	'Webradio Rock':		('rock','239.231.140.168','1234',['http://webradio.crans.org:8000/rock.mp3']),
	'I.ACTIVE DANCE':		('iactive','239.231.140.170', '1234', ['http://serveur.wanastream.com:48700/']),
	'Skyrock':			('skyrock', '239.231.140.171', '1234', ['http://95.81.146.6/3665/sky_122353.mp3']),
        'Rire et Chanson':		('rireetchanson', '239.231.140.172', '1234', ['http://95.81.146.10/5011/nrj_122230.mp3']),
	'Europe 1':			('europe1', '239.231.140.173', '1234', ['http://vipicecast.yacast.net/europe1.mp3']),
	'Chérie FM':			('cherie_fm', '239.231.140.174', '1234', ['http://95.81.146.2/cherie_fm/all/che_124310.mp3']),
	'France Culture':		('culture', '239.231.140.175', '1234', ['http://95.81.147.3/franceculture/all/franceculturehautdebit.mp3']),
	'BFM':				('bfm', '239.231.140.176', '1234', ['http://vipicecast.yacast.net/bfm.mp3']),
	'France Musique':		('musique', '239.231.140.177', '1234', ['http://95.81.147.3/francemusique/all/francemusiquehautdebit.mp3']),
	'Fun Radio':			('funradio', '239.231.140.178', '1234', ['http://streaming.radio.funradio.fr/fun-1-44-128.mp3']),
	'Nostalgie':			('nostalgie', '239.231.140.179', '1234', ['http://95.81.146.10/5010/nrj_121955.mp3']),
	'le mouv\'':			('lemouv', '239.231.140.180', '1234', ['http://95.81.147.3/lemouv/all/lemouvhautdebit.mp3']),
	'NRJ':				('nrj', '239.231.140.181', '1234', ['http://95.81.146.2/nrj/all/nrj_113225.mp3']),
	'rts':				('rtd', '239.231.140.182', '1234', ['http://95.81.146.6/3966/rtsfm_hd.mp3']),
	'Sud Radio':			('sud_radio', '239.231.140.183', '1234', ['http://95.81.147.10/5726/gie_105741.mp3']),
	'France Bleu': 			('bleu', '239.231.140.184', '1234', ['http://95.81.146.2/fbidf/all/fbidfhautdebit.mp3']),
	'RFM':				('rfm', '239.231.140.185', '1234', ['http://vipicecast.yacast.net/rfm_128']),
	'RTL':				('rtl', '239.231.140.186', '1234', ['http://streaming.radio.rtl.fr/rtl-1-44-128']),
	'RTL2':				('rtl2', '239.231.140.187', '1234', ['http://streaming.radio.rtl2.fr/rtl2-1-44-96']),
	
	},
}

for i in range(1, 5):
    multicast['Radio']['BBC Radio %s' % i]=('bbc%s' % i, '239.231.140.19%s' % i, '1234', ['http://bbcmedia.ic.llnwd.net/stream/bbcmedia_intl_lc_radio%s_p' % i ])
