#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Génère un graphe de dépendance des groups bcfg2

Auteurs: Daniel Stan
         Valentin Samir

"""
from __future__ import print_function
import xml.dom.minidom
import subprocess

GRAPH_BIN = '/usr/bin/neato'
TMP_FILE = '/tmp/graph.gv'
GROUPS_FILE = '/var/lib/bcfg2/Metadata/groups.xml'
OUTPUT_FILE = '/usr/scripts/var/doc/bcfg2/groups.svg'

out = file(TMP_FILE, 'w')

import datetime

# groups est le nœud racine
groups = xml.dom.minidom.parse(file(GROUPS_FILE,'r')).documentElement

print("""digraph G {
edge [len=4.50, ratio=fill]
""", file=out)

def childGroups(parent):
    """Récupère les groups enfants (dépendances) d'un nœud
    Attention, cette fonction travaille sur (et renvoie) des nœuds xml
    """
    return [ x for x in parent.childNodes if \
             x.nodeType == x.ELEMENT_NODE and x.tagName == u'Group']

# Les clients (ie des serveurs)
# sont coloriés d'une autre couleur
print("""
subgraph cluster_1 {
        node [style=filled];
""", file=out)
for elem in childGroups(groups):
    if elem.hasAttribute('profile'):
        print('"%s";' % elem.getAttribute('name'), file=out)
print("""
        label = "process #2";
        color=blue
    }
""", file=out)
# Le reste

for elem in childGroups(groups):
    print('"%s" -> {%s};' % \
         (  elem.getAttribute('name'),
             " ".join( [ '"%s"' % x.getAttribute('name')
                            for x in childGroups(elem) ]),
                            ), file=out)

print("""
label = "\\n\\nBCFG2 Groups\\nLes Nounous\\n%s";
}""" % datetime.datetime.now().strftime('%c'), file=out)

out.close()

subprocess.Popen([GRAPH_BIN, "-Tsvg", TMP_FILE, "-o", OUTPUT_FILE]).communicate()
