#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-
# Draf de code pour alerter les adhérents de leur fin de connexion imminente
# Ce script devra aussi permettre d'alerter les cableurs sur les prochaines
# affluences en perm

import sys
import pytz
import datetime

from lc_ldap.shortcuts import lc_ldap_readonly
from lc_ldap.variables import base_dn
import ldap

FORMAT_LDAP = '%Y%m%d%H%M%S%z'

#: filtre ldap max(finConnexion) \in intervalle
# NB: finConnexion est un attribut ldap multivalué, et on s'intéresse ici
# à sa valeur max pour un adhérent.
# Les filtres ldap recherchent, de manière existentielle, une valeur valide.
# Ainsi, en notant F l'ensemble des valeurs : 
# max(F) >= v  <=>  \exists x\in F x>= v   <=>   finConnexion>=v
# L'autre inégalité ( < ) est plus délicate :
# max(F) < v   <=>   \forall x\in F x < v   <=>   \not( \exists x\in F x >= v )
#                <=> \not(  finConnexion>= v )
filtre_tpl = u'(&(finConnexion>=%s)(!(finConnexion>=%s)))'

try:
    with open('/etc/timezone', 'r') as f:
        tz = pytz.timezone(f.read().strip())
except:
    tz = pytz.UTC

#: Périodicité du script (pour n'envoyer le mail qu'une fois)
periodicite = datetime.timedelta(days=7)

#: Dans combien de temps la connexion aura-t-elle expiré ?
expire_delay = datetime.timedelta(days=21)

c = lc_ldap_readonly()

# Instant courant: on ne garde que le jour
to_erase = { 'second': 0, 'minute': 0, 'microsecond': 0, 'hour': 0, }
now = datetime.datetime.now(tz).replace(**to_erase)

# Applique un delta, si spécifié
for arg in sys.argv[1:]:
    if arg.startswith('+'):
        now += datetime.timedelta(days=int(arg[1:]))
print "Nous serons le %s" % now

fin = now + expire_delay
warn_debut = fin - periodicite

filtre = filtre_tpl % tuple(d.strftime(FORMAT_LDAP) for d in [warn_debut, fin] )

# NB: on ne prend que les adhérents, d'où SCOPE_ONELEVEL
to_warn = c.search(filtre, scope=ldap.SCOPE_ONELEVEL, dn=base_dn)


print ("%d adhérents seront prévenu que leur connexion expire entre le %s " + \
    "et le %s") % (len(to_warn), warn_debut, fin)

if "--list" in sys.argv:
    for adh in to_warn:
        print repr(adh) + ' ' + adh.dn.split(',', 1)[0]
