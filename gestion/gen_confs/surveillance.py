#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Classe de synchronisation entre la base ldap et
la base postgresql pour la liste des exemptions
et la liste des machines.
Utilisé par generate.py
"""

# importation des fonctions et classes

import sys, os, commands
sys.path.append('/usr/scripts/gestion')
from ldap_crans import crans_ldap, Club, Adherent, AssociationCrans
from gen_confs import gen_config
import psycopg2
import netaddr

db = crans_ldap()

# Génération des la tables d'exemptions
#######################################

class exemptions(gen_config) :
    
    base=[('138.231.136.0/28','0.0.0.0/0'),('138.231.136.0/21','138.231.0.0/16'),('138.231.148.0/22','138.231.0.0/16')]
    restart_cmd = ""
    
    def __str__ (self) :
        return "surveillance_exemptions"
    
    def _gen (self) :
        
        machines = db.search('exempt=*')['machine']
        pgsql = psycopg2.connect(database='filtrage', user='crans')
        curseur = pgsql.cursor()
        
        requete="DELETE FROM exemptes"
        curseur.execute(requete)
        
        for source,destination in self.base :
            requete="INSERT INTO exemptes (ip_crans,ip_dest) VALUES ('%s','%s')" % (source,destination)
            curseur.execute(requete)
            
        for machine in machines :
            for destination in machine.exempt() :
                if netaddr.IPNetwork(destination).version == 4:
                    source = machine.ip()
                else:
                    source = machine.ipv6()
                # Si ip vide, passons au suivant
                if not source:
                    continue
                requete="INSERT INTO exemptes (ip_crans,ip_dest) VALUES ('%s','%s')" % (source, destination)
                curseur.execute(requete)
        
        pgsql.commit()

# Génération des la liste des machines
######################################

class machines(gen_config) :
    
    restart_cmd = ""
    
    def __str__ (self) :
        return "surveillance_liste_machines"
            
    def _gen (self) :
                
        # recherche de toutes les machines dans ldap
        import sys
        sys.path.append('/usr/scripts/gestion')
        machines = db.search('ip=*')['machine']
        
        # liste des machines (on prend que les paimement ok)
        # machines = self.machines
        # machines = db.search('ip=*')['machine']
        
        # connexion à la base postgresql
        import psycopg2
        pgsql = psycopg2.connect(database='filtrage', user='crans')
        curseur = pgsql.cursor()
        curseur.execute("DELETE FROM machines;")

        ipv6_vu={}
        def ipv6_already_set(ipv6):
            # S'il ne s'agit pas d'une IP valide (vide ?) faisons comme si
            # on l'avait déjà vue :p
            if not ipv6:
                return True
            ret = ipv6_vu.get(ipv6, False)
            ipv6_vu[ipv6] = True
            return ret
        
        # ajout des entrée
        for m in machines:
            if m.proprietaire().__class__ == Club:
                curseur.execute("INSERT INTO machines (ip, type, id) VALUES (inet'%s','club',%s);"%(m.ip(),m.proprietaire().id()))
                if not ipv6_already_set(m.ipv6()):
                    curseur.execute("INSERT INTO machines (ip, type, id) VALUES (inet'%s','club',%s);"%(m.ipv6(),m.proprietaire().id()))
            elif m.proprietaire().__class__ == Adherent:
                curseur.execute("INSERT INTO machines (ip, type, id) VALUES (inet'%s','adherent',%s);"%(m.ip(),m.proprietaire().id()))
                if not ipv6_already_set(m.ipv6()):
                    curseur.execute("INSERT INTO machines (ip, type, id) VALUES (inet'%s','adherent',%s);"%(m.ipv6(),m.proprietaire().id()))
            elif m.proprietaire().__class__ == AssociationCrans:
                curseur.execute("INSERT INTO machines (ip, type, id) VALUES (inet'%s','crans',%s);"%(m.ip(),m.id()))
                if not ipv6_already_set(m.ipv6()):
                    curseur.execute("INSERT INTO machines (ip, type, id) VALUES (inet'%s','crans',%s);"%(m.ipv6(),m.id()))
        # on commit
        pgsql.commit()
        
