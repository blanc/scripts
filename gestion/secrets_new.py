# -*- coding: utf-8 -*-
#
# secrets.py
# ----------
#
# Copyright (C) 2007 Jeremie Dimino <dimino@crans.org>
#
# This file is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.

"""
Recuperation des secrets depuis /etc/crans/secrets.
"""

import sys
import os
import logging
import logging.handlers
import getpass

# Initialisation d'un logger pour faire des stats etc
# pour l'instant, on centralise tout sur thot en mode debug
logger = logging.getLogger('secrets_new')
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(name)s: [%(levelname)s] %(message)s')
handler = logging.handlers.SysLogHandler(address = '/dev/log')
try:
    handler.addFormatter(formatter)
except AttributeError:
    handler.formatter = formatter
logger.addHandler(handler)

def get(secret):
    """ Récupère un secret. """
    prog = os.path.basename(getattr(sys, 'argv', ['undefined'])[0])
    logger.debug('%s (in %s) asked for %s' % (getpass.getuser(), prog, secret))
    try:
        f = open("/etc/crans/secrets/" + secret)
        result = f.read().strip()
        f.close()
        return result
    except:
        try:
            sys.path.insert(0, '/etc/crans/secrets')
            import secrets as module
            sys.path.pop(0)
            return getattr(module, secret)
        except:
            logger.critical('...and that failed.')
            if os.getenv('DEBUG', 0):
                raise
            else:
                raise Exception("Impossible d'acceder au secret %s!" % secret)
