#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Fonctions de tests sur l'utilisateur

Copyright (C) Frédéric Pauget
Licence : GPLv2
"""

import os, commands, pwd

def getuser() :
    """ Retourne l'utilisateur lancant les scripts """
    user = os.getenv('SUDO_USER')
    if not user :
        user = pwd.getpwuid(os.getuid())[0]
    return user

def groups(login='') :
    """ Retourne la liste des droits qu'a l'utilisateur fourni, si aucun
    utilisateur n'est fourni prend l'utilisateur loggué """

    if login == '':
        login = getuser()


    if login == 'root':
        groups = ['0']
    else:
        import sys
        sys.path.append('/usr/scripts/gestion')
        from ldap_crans import crans_ldap

        try:
            adh = crans_ldap().search('uid=%s' % login)['adherent'][0]
        except:
            groups = []
        else:
            groups = adh.droits()

    return groups


def isadm(login='') :
    """ Retourne True si l'utilisateur est dans le groupe 4 (adm)
    Si login='', prend l'utilisateur loggué """
    return u'Nounou' in groups(login) or '0' in groups(login)

def isdeconnecteur(login='') :
    """ Retourne True si l'utilisateur est dans le groupe 611 (bureau)
    Si login='', prend l'utilisateur loggué """
    return isadm(login) or u'Bureau' in groups(login)

def iscableur(login=''):
    """ Retourne True si l'utilisateur est dans le groupe 604 (respbat)
    Si login='', prend l'utilisateur loggué """
    return isadm(login) or u'Cableur' in groups(login)
