#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-
#
# forward.py
# -----------------
#
# Copyright (C) 2013,2014: Raphaël-David Lasseri <lasseri@crans.org>,
#               Valentin Samir <samir@crans.org>,
# This file is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.

import sys, re, datetime, string
import os
import argparse
from affich_tools import coul, prompt
import affich_tools
import lc_ldap.shortcuts
import lc_ldap.printing
import lc_ldap.filter2 as filter


if __name__ == '__main__':
# Lecture de la redirection courante
    def redirection_read(uid):
        forward = open(os.path.expanduser('~%s/.forward' %uid) ,'r')
    print forward.read()
    forward.close()
    return
# Modification de la redirection
    def redirection_write(ligne,uid):
    forward = open(os.path.expanduser('~%s/.forward' %uid),'w')
    forward.write('%s' %ligne)
    forward.close()
    return

# On reprends whos_lc() de Valentin
    if len(sys.argv) >1:
        conn=lc_ldap.shortcuts.lc_ldap_admin()
        result=conn.search(filter.human_to_ldap(sys.argv[1]), sizelimit=4000)
        if not result:
            print "Aucun résultat !"
        exit()
        else:
            if len(result) == 1:
                if len(result[0].get(u'droits',None))==0:
            result[0].display()
            uid=result[0].get(u'uid',None)[0].value
        if len(result[0].get(u'droits',None))!=0:
            print 'Membre actif !'
            exit()
            else:
                print lc_ldap.printing.sprint_list(result)
                print "%s résultats" % len(result)
        exit()
    else:
        sys.stderr.write("Utiliser les filtres ldap: uid=, chbre= etc..")
    exit()

r = affich_tools.prompt(u'Confirmer sélection ? [O/N]')
if r == 'O' or r == 'o':
    print "Redirection actuelle:"
    redirection_read(uid)
    y = affich_tools.prompt(u'Modifier cette redirection ? [O/N]')
    if y == 'O' or y == 'o':
        ligne = affich_tools.prompt(u'Nouvelle redirection:')
        redirection_write(ligne,uid)
        print 'Redirection effectuée !'
    elif  y == 'N' or y == 'n':
        exit()
elif r == 'N' or r == 'n':
    exit()




