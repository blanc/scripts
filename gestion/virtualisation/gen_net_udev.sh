#!/bin/bash


# Sur certains virtualiseurs xen, les interfaces xen n'étaient pas dans udev.
# Voici un petit script qui écrit les règles correspondantes

ip -o l show | sed 's/^[0-9]*: \(eth[^:]*\).*ether \([^ ]*\) .*$/SUBSYSTEM=="net", DRIVERS=="?*", ATTR{address}=="\2", NAME="\1"/; t; d'

