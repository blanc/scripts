#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-
#
# Basic trigger host, will be imported from any other
# Contains a TriggerFactory, which records the host functions
# decorated with @record.
# Contains a trigger which calls good functions from factory.
#
# Author    : Pierre-Elliott Bécue <becue@crans.org>
# License   : GPLv3
# Date      : 28/04/2014

import collections

class TriggerFactory(object):
    """Factory containing which function is part of the trigger set

    """

    _services = {}
    _parsers = collections.defaultdict(list)

    @classmethod
    def register_service(cls, key, value):
        cls._services[key] = value

    @classmethod
    def get_service(cls, key):
        return cls._services.get(key, None)

    @classmethod
    def get_services(cls):
        return cls._services.values()

    @classmethod
    def register_parser(cls, keys, parser):
        for key in keys:
            cls._parsers[key].append(parser)

    @classmethod
    def get_parser(cls, keyword):
        return cls._parsers[keyword]

def record_service(func):
    """Records in the triggerfactory the function

    The function provided are services to regen

    """
    TriggerFactory.register_service(func.func_name, func)

def trigger_service(what):
    return TriggerFactory.get_service(what)

def record_parser(*args):
    def find_parser(func):
        TriggerFactory.register_parser(args, func)
        return func
    return find_parser
