#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Trigger library config file
# License : GPLv3

import itertools

debug = True
log_level = "info"

# Serveur maître
master = "rabbitmq.adm.crans.org"
user = "trigger"
port = 5671
ssl = True

# Liste des services associés aux hôtes
# useradd : Envoie le mail de bienvenue, et crée le home
# userdel : Détruit le home, déconnecte l'utilisateur sur zamok, détruit les indexes dovecot, désinscrit l'adresse crans des mailing listes associées
services = {
            'civet'     : ["event"],
            'dhcp'      : ["dhcp"],
            'dyson'     : ["autostatus"],
            'isc'       : ["dhcp"],
            'komaz'     : ["firewall", "secours"],
            'owl'       : ["userdel"],
            'redisdead' : ["mailman", "modif_ldap", "solde", "userdel", "secours"],
            'sable'     : ["dns"],
            'titanic'   : ["secours"],
            'zamok'     : ["userdel"],
            'zbee'      : ["useradd", "userdel"],
        }

# XXX - Uncomment this when in prod
#all_services = set([service for service in itertools.chain(*services.values())])

all_services = ['dhcp', 'firewall']
