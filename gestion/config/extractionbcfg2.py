#!/usr/bin/python
# -*- coding: utf-8 -*-
# © Olivier Huber, 2010
# Licence MIT
""" Ce script va chercher la correspondance entre des attributs et des serveurs"""

from lxml import etree

template = """# -*- python -*-
# -*- coding: utf-8 -*-

# Généré par gestion/extractionbcfg2.py sur bcfg2, à partir de Metadata/groups.xml

adm_only = %(adm_list)s

role = %(role_dict)s
"""

srvDict = { 'external'              : [],
             'connection-main'       : [],
#             'failover-proxy-server' : [],
             'vlan-ens'              : [],
             'users'                 : [],
             'sniffer'               : [],
             'router-wifi'           : [],
             'routeur-nat64'         : [],
}

tr = { 'external'              : 'externe',
       'connection-main'       : 'main-router',
       'vlan-ens'              : 'appt-proxy',
#       'failover-proxy-server' : 'failover-proxy',
       'router-wifi'           : 'wifi-router',
       'users'                 : 'adherents-server',
       'sniffer'               : 'sniffer',
       'routeur-nat64'         : 'routeur-nat64',
}

fin = open('/var/lib/bcfg2/Metadata/groups.xml')

# La gestion des exceptions est laissée en exercice pour ceux qui vont
# casser Metadata/groups.xml
parsedGroups = etree.parse(fin)

for key in srvDict.keys():
    srvDict[key] = parsedGroups.xpath('//Group[@name="%s"]/../@name' % key)
    if srvDict[key] == []:
        print "Erreur, il n'y a pas de serveur associé à l'attribut %s" % key
        exit(1)

# On cherche les serveurs qui sont seulement sur le vlan adm
admOnly = parsedGroups.xpath('//Group[@name="adm-only"]/../@name')

fin.close()

out = {}
for key, values in srvDict.items():
    for value in values:
        if value in out.keys():
            out[value].append(tr[key])
        else:
            out[value] = [tr[key]]

fout = open('/usr/scripts/gestion/config/config_srv.py', 'w')

fout.write(template % {'adm_list': admOnly.__str__(), 'role_dict' : out.__str__()})

fout.close()

