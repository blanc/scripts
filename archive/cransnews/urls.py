from django.conf.urls.defaults import *
from cransnews.news2rss import DernieresAnnonces, LatestMessages

feeds = {
    'group': LatestMessages,
    'annonces': DernieresAnnonces,
}


urlpatterns = patterns('',
    # Example:
    # (r'^cransnews/', include('cransnews.foo.urls')),
    #(r'^rss/(?P<group>[\.\w]+)', 'news2rss.views.news2rss'),
    (r'^rss/(?P<url>.*)/$', 'django.contrib.syndication.views.feed', {'feed_dict': feeds}),

    # Uncomment this for admin:
#     (r'^admin/', include('django.contrib.admin.urls')),
)
