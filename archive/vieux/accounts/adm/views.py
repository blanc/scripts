# Create your views here.
from accounts.cransaccounts.models import Credential
from django.http import HttpResponse, HttpResponseRedirect
import datetime
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.template import RequestContext

def check_credential( request ):
    cred = request.GET['cred']
    service = request.GET['service']
    response = HttpResponse("INVALID") 
    # retreive credential in db
    try:
        cred = Credential.objects.get(token=cred, service=service)
        ## if not to old, accept
        if cred.date.today() - cred.date < datetime.timedelta( minutes=10 ):
            response = HttpResponse("VALID")
            response['username'] = cred.user.username
            response['fullname'] = cred.user.get_full_name()
            response['email'] = cred.user.email
        else:
            response = HttpResponse("INVALID")
    except Credential.DoesNotExist:
        response = HttpResponse("INVALID") 
    response["content-type"] = "text/plain"
    return response
    ## else, or if not exist reject
