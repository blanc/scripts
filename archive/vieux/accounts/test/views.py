# Create your views here.
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse

def index(request):
    return HttpResponse("Hello, world. You're at the poll index.")
    
def my_view(request): 
    return HttpResponse("youpla ! (you're logged in ;-))")
my_view = login_required(my_view)