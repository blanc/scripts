from sqlobject import *

class Local(SQLObject):
    
    nom = StringCol(unique=True)
    proprio = ForeignKey('Proprio')
    batiment = ForeignKey('Batiment')
    
class Batiment(SQLObject):
    
    nom = StringCol(unique=True)
    
