
import logging

import sys
sys.path.append('/usr/scripts/')
from cranslib.utils.logs import getFileLogger
LOGGER = getFileLogger("helloworld")
LOGGER.setLevel(logging.INFO)
LOGGER.addHandler(logging.StreamHandler())

if __name__ == "__main__":
    LOGGER.info(u"Hello World")
