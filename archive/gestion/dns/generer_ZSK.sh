#!/bin/bash
# Generation d'une nouvelle clef ZSK pour une zone

DATE=`date --utc +%s`

cd /usr/scripts/var/dnssec

if [[ $1 == "" ]]
    then echo "Usage: $0 nom_de_la_zone"
         echo "Exemple: $0 wifi.crans.org"
         exit
fi


echo "Generation nouvelle ZSK pour $1"
dnssec-keygen -r /dev/urandom -a RSASHA256 -b 2048 -n ZONE $1

## On renomme de façon utilisable
mv K$1.+*.key K$1.$DATE.ZSK.key
mv K$1.+*.private K$1.$DATE.ZSK.private

## On met a jour les liens symboliques vers la clef actuelle
rm K$1.ZSK.key
rm K$1.ZSK.private

ln -s K$1.$DATE.ZSK.key K$1.ZSK.key
ln -s K$1.$DATE.ZSK.private K$1.ZSK.private

chmod 660 K*

## On met à jour le fichier inclu dans la zone
bash /usr/scripts/gestion/dns/generer_include_zone.sh $1
