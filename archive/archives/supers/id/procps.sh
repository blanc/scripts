#! /bin/sh
# Sam K, Jan 2000
# sert Doublement de wrapper:
# d'abord autolance super sur /id/(basename $0),
# Et aussi, qd appel� via super, lance un scipt de /id en modifiant le apth au prealable.

prog=`basename $0`
supcmd=`basename ${SUPERCMD=_NILL_}`

# Si le script courant est lanc<E9> par super, $SUPERCMD permet de le voir.
# et alors on continue gentiment le script, sinon on <E9>x<E9>cute super sur le prog.
#  ca permet de faire des scripts qui lancent super sur eux-memes..
if ! test "X$supcmd" = "X$prog"; then
	echo "calling super, $supcmd $prog"
	exec /usr/bin/super /id/$prog ${1+"$@"}
	exit 0;
fi

# Si le script continue, c'est qu'on est lanc� par super.


PATH=/sbin:/usr/sbin:/usr/bin:$PATH

echo "Path set to: $PATH  by id_wrapper"
exec /etc/init.d/$prog ${1+"$@"}
