#! /usr/bin/env python
# -*- coding: iso-8859-15 -*-

# les mots de passe sont crypt�s avec la commande :
# python -c "import sha ; print sha.new('***').hexdigest()"

# Liste des utilisateurs
########################
# premier champ   : autorisation d'inscrire des machines temporaires True|False
# champs suivants : pr�fixe des lieux

users = {
  'bilou:b6831110716ea7782b636469b31dc3a695b26386'   : ['ens'],
  'vince||:7bc07c05eebf6726b48f557fcb60b434364034cd' : ['ens'],
  'xabi:4f1da4cacfd69622c2123d83007a92f9e3de9722'    : ['ens'],
  # Jean-Marc Roussel, pour le laboratoire d'automtique du dgm
  'labo_auto:920eb1d6bc608a3e8d3a20ccc49bee6c849ccb8b': ['ens_vinci_autom'],
  # C�cile Durieu
  'durieu:897712550705c3e02e795e3eea579b0e40c90903'   : ['ens_alembert'],
  # farid.benboudjema, pour le DGC
  'farid:c710e92d2d15f292f2d5f8c5901fcf91a778590a'    : ['ens_vinci_dgc']
}

# 
