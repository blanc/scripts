#! /usr/bin/env python
# -*- coding: iso-8859-15 -*-

""" D�fini les transpondeurs � associer � chacune des cartes """

from sat_base import *
from socket import gethostname

host = gethostname()

transpondeurs = { 'canard' : [ Hotbird_12476(0) ,
                               Hotbird_11240(1) ,
                               Hotbird_12597(2) ,
                               TNT_R4_498000(4) ,
                               Hotbird_11604(3) ],
                  'lapin' :  [ Hotbird_11727(0) ,
#                               Hotbird_11623(1) , #Cette carte capte tres mal
                               TNT_R1_586000(2) ,
                               TNT_R6_562000(3) ,
                               TNT_R2_474000(4) ],
                  'oie' : [    Hotbird_10853(0) ,
                               Hotbird_11766(1) ,
                               Hotbird_12111(2) ,
                               Hotbird_11137(3) ,
                               TNT_R7_490000(4) ,
#                               TNT_R5_538000(4) ,
                               TNT_R3_522000(5) ]
                               }

conf = transpondeurs.get(host,[])
                               
if __name__ == '__main__' :
    import sys
    if len(sys.argv) == 2 :
        conf = transpondeurs.get(sys.argv[1],[])
    for t in conf :
        print t, "sur la carte", t.card
        for (sap_group, chaine) in t.chaines.values() :
            print '\t%s\t: \t%s' % (sap_group,chaine)
