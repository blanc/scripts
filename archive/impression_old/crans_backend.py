#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-

# Fonctions utilis�es par les backend devis et laserjet
# �crit par Benoit avec la contribution de Brice et Fred

# Licence : GNU General Public Licence, version 2

import sys, tempfile
import couts
sys.path.append('/usr/scripts/gestion')
from ldap_crans import crans_ldap


def preliminaires(arguments, description):
    """ Verifie le nombre d'arguments passes.
    Si pas d'arguments : print description,
    si le bon nombre d'argument : retourne le fichier nettoye,
    sinon sort sans stopper.
    """

    # \x1B est le caractere d'�chappement �chap.
    UEL="\x1B%-12345X"

    if len(arguments) == 1 :
        print description
        sys.exit(0)

    if len(arguments) not in (6, 7) :
        sys.stderr.write("ERROR: %s job-id user title copies options [file]\n" % arguments[0])
        sys.exit(0) # On n'arr�te pas l'imprimante

    if arguments[-1].find('/') != -1 :
        try:
            # Fichier fourni
            entree = open(arguments[-1])
        except:
            # Des fois les options contiennent un / ...
            entree = sys.stdin
    else :
        # Lecture sur entr�e standard
        entree = sys.stdin

    ## On nettoie le fichier :
    fic = tempfile.NamedTemporaryFile()
    ligne = entree.readline()
    # On enl�ve les options qui nous g�nent et on rend le fichier propre
    while ligne:
        # On ne choisit pas le bac de sortie
        if ligne.find('%%BeginFeature: *OutputBin') != -1:
            while ligne.find('%%EndFeature') == -1:
                ligne = entree.readline()
            ligne = entree.readline()
        # On ne choisit pas le bac d'entree sauf si transparent
        if ligne.find('%%BeginFeature: *InputSlot') != -1:
            if ligne.find('Transparency') == -1:
                while ligne.find('%%EndFeature') == -1:
                    ligne = entree.readline()
                ligne = entree.readline()
        # On �limine les instructions PJL
        # Normalement on n'est pas oblig� de retirer le UEL
        # mais c'est plus propre
        while ligne.startswith('@PJL') or ligne.startswith(UEL):
            ligne = entree.readline()
        fic.write(ligne)
        ligne = entree.readline()
    fic.flush()
    return fic


def utilisateur(user, rw):
    """ Renvoie l'adherent qui imprime le job
    * user est l'utilisateur (argument n�2 des param�tres pass�s au backend)
    * rw vaut 1 si on veut modifier le compte LDAP de l'adh�rent,
      0 dans le cas contraire
    """

    # Impression en local avec les certificats (possible pour le groupe adm)
    if user=="root":
        sys.stderr.write("ERROR: Utilisateur root pass� en param�tre.\n")
        sys.exit(0) # On conclue l'impression

    # R�cup�ration de l'adh�rent
    try:
        base = crans_ldap()
        if rw == 0 :
            res=base.search("login=%s"%user)['adherent']
        else:
            res=base.search("login=%s"%user,'w')['adherent']
    except:
        sys.stderr.write("ERROR: Erreur : Base LDAP non joignable\n")
        sys.exit(0) # On arrete l'impression

    # Si on ne trouve rien :
    if len(res) != 1 :
        sys.stderr.write("ERROR: Erreur : adh�rent %s non trouv�\n" % user)
        sys.exit(0) # On conclue l'impression sans stopper l'imprimante

    adherent = res[0]
    sys.stderr.write("DEBUG: Adherent %s recupere.\n" % adherent.Nom())
    return adherent


def calcul_prix(nom_fic):
    """Calcul le prix du fichier nom_fic"""

##     #Temporaire pour debugage
##     fs=open(nom_fic,'r')
##     fd=open('/tmp/benoit_Routard','w')
##     while 1:
##         txt=fs.readline()
##         if txt == '':
##             break
##         fd.write(txt)
##     fs.close()
##     fd.close()

    # Calcul du cout de l'impression :
    try:
        prix = couts.cout(nom_fic)
    except:
        sys.stderr.write("ERROR: Erreur : Impossible de calculer le couts de %s." % nom_fic)
        sys.exit(1) # On arrete l'imprimante

    if prix.erreur == "Taille invalide":
        sys.stderr.write("ERROR: Erreur de taille de papier (%s).\n" % prix.taille)
    elif prix.erreur:
        sys.stderr.write("ERROR: Erreur du calcul du prix : %s.\n" % prix.erreur)
    else:
        sys.stderr.write("DEBUG: Prix calcule : %s euros (%s, %s).\n" % (prix.c_total_euros, prix.taille, prix.recto_v) )

    return prix


def send_email(sender, recipient, subject, body):
    """Send an email.

    All arguments should be Unicode strings (plain ASCII works as well).

    Only the real name part of sender and recipient addresses may contain
    non-ASCII characters.

    The email will be properly MIME encoded and delivered though SMTP to
    localhost port 25.  This is easy to change if you want something different.

    The charset of the email will be the first one out of US-ASCII, ISO-8859-1
    and UTF-8 that can represent all the characters occurring in the email.
    """
    from smtplib import SMTP
    from email.MIMEText import MIMEText
    from email.Header import Header
    from email.Utils import parseaddr, formataddr

    # Header class is smart enough to try US-ASCII, then the charset we
    # provide, then fall back to UTF-8.
    header_charset = 'ISO-8859-1'

    # We must choose the body charset manually
    for body_charset in 'US-ASCII', 'ISO-8859-1', 'UTF-8':
        try:
            body.encode(body_charset)
        except UnicodeError:
            pass
        else:
            break

    # Split real name (which is optional) and email address parts
    sender_name, sender_addr = parseaddr(sender)
    recipient_name, recipient_addr = parseaddr(recipient)

    # We must always pass Unicode strings to Header, otherwise it will
    # use RFC 2047 encoding even on plain ASCII strings.
    sender_name = str(Header(unicode(sender_name), header_charset))
    recipient_name = str(Header(unicode(recipient_name), header_charset))

    # Make sure email addresses do not contain non-ASCII characters
    sender_addr = sender_addr.encode('ascii')
    recipient_addr = recipient_addr.encode('ascii')

    # Create the message ('plain' stands for Content-Type: text/plain)
    msg = MIMEText(body.encode(body_charset), 'plain', body_charset)
    msg['From'] = formataddr((sender_name, sender_addr))
    msg['To'] = formataddr((recipient_name, recipient_addr))
    msg['Subject'] = Header(unicode(subject), header_charset)

    # Send the message via SMTP to localhost:25
    smtp = SMTP("localhost")
    smtp.sendmail(sender, recipient, msg.as_string())
    smtp.quit()
