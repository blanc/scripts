import crans.impression
import crans.impression.digicode
import crans.ldap_crans_test
import os, sys
# ######################################################## #
#                 COMMAND LINE OPTION                      #
# ######################################################## #
#
#
OPTIONS_AGRAFES = {
    0: crans.impression.PAS_D_AGRAPHES,
    -1: crans.impression.AGRAPHE_DIAGONALE,
    1: crans.impression.UNE_AGRAPHE,
    2: crans.impression.DEUX_AGRAPHE,
    3: crans.impression.TROIS_AGRAPHE,
    6: crans.impression.STITCHING,
}
OPTIONS_NOIRETBLANC = {
	False: crans.impression.IMPRESSION_COULEUR,
	True: crans.impression.IMPRESSION_NB
}
OPTIONS_RECTOVERSO = {
	False: crans.impression.IMPRESSION_RECTO,
	True: crans.impression.IMPRESSION_RECTO_VERSO
}

from optparse import OptionParser

parser = OptionParser("usage: %prog [options] pdf")
parser.add_option("-a", "--agrafes",
    action="store", type='int', dest="agrafes", default=0,
    help="Choix du mode d'agrafes (%s)" % ", ".join(["%s: %s" % (val, crans.impression.LABELS[OPTIONS_AGRAFES[val]]) for val in OPTIONS_AGRAFES.keys()]))
parser.add_option("-p", "--papier",
    action="store", type="string", dest="typepapier",
    help="Choix papier (%s)" % ", ".join(["%s: %s" % (val, crans.impression.LABELS[val]) for val in crans.impression.PAPIER_VALEURS_POSSIBLES]))
parser.add_option("-r", "--recto-verso",
    action="store_true", dest="rectoverso", default=False,
    help="Impression recto-verso")
parser.add_option("-c", "--copies",
    action="store", type="int", dest="copies",
    help="Nombre de copies")

parser.add_option("-n", "--noir-et-blanc",
    action="store_true", dest="noiretblanc", default=False,
    help="impression en noir et blanc")


(options, args) = parser.parse_args()

if len(args) != 1:
    parser.error("Nombre d'arguments incorect")
PDF_PATH = os.path.join(os.getcwd(), args[0])
USER_LOGIN =  os.getlogin()

print("Analyse du fichier...")
lpr = crans.impression.impression(PDF_PATH, USER_LOGIN)

try:
	lpr.changeSettings(agraphes=OPTIONS_AGRAFES[options.agrafes],
					papier=options.typepapier,
					couleurs=OPTIONS_NOIRETBLANC[options.noiretblanc],
					recto_verso = OPTIONS_RECTOVERSO[options.rectoverso],
					copies=options.copies )
except crans.impression.SettingsError, e:
	print "erreur: %s" % e
	sys.exit(1)

lpr.printSettings()

print "Prix total : %s Euros" % str(lpr.prix())
