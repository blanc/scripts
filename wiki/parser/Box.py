# -*- coding: iso-8859-1 -*-
"""
                                            ..
                       ....  ............   ........
                     .     .......   .            ....  ..
                   .  ... ..   ..   ..    ..   ..... .  ..
                   .. .. ....@@@.  ..  .       ........  .
              ..  .  .. ..@.@@..@@.  .@@@@@@@   @@@@@@. ....
         .@@@@. .@@@@. .@@@@..@@.@@..@@@..@@@..@@@@.... ....
       @@@@... .@@@.. @@ @@  .@..@@..@@...@@@.  .@@@@@.    ..
     .@@@..  . @@@.   @@.@@..@@.@@..@@@   @@ .@@@@@@..  .....
    ...@@@.... @@@    .@@.......... ........ .....        ..
   . ..@@@@.. .         .@@@@.   .. .......  . .............
  .   ..   ....           ..     .. . ... ....
 .    .       ....   ............. .. ...
 ..  ..  ...   ........ ...      ...
  ................................

    MoinMoin - Box parser

    PURPOSE:
        une boite jolie

    CALLING SEQUENCE:
      {{{
      #!Box titre
      blablabla

      tables, images....
      }}}

    Copyright 2008,2009   Antoine Durand-Gasselin <adg@crans.org>

"""

import random
from MoinMoin import config, wikiutil

Dependencies = []
color_list = ['orange', 'black', 'red', 'green', 'blue', 'gray']

#####################################################################
# Parser :  classe principale, c'est elle qui fait tout
#######
class Parser:
    parsername = "Box"
    Dependencies=Dependencies

    def __init__(self, raw, request, **kw):
        self.request = request
        self.raw = raw
        self.settings = self.parseArgs(kw["format_args"])
        self.parser = wikiutil.searchAndImportPlugin(request.cfg, "parser", request.page.pi['format'])

    def parseArgs(self, argsString):
        argList = argsString.split(u',')
        settings = {}
        for anArg in argList:
            if anArg.find(u'color=')!=-1:
                theColor = anArg.split(u'=')[1].lower().strip()
                if theColor == 'random':
                    theColor = random.choice(color_list)
                settings['color'] = theColor
            else:
                settings['title'] = anArg.strip()
        return settings

    def format(self, formatter):
        content = wikiutil.renderText(self.request, self.parser, self.raw)

        title = self.settings.get('title', '')
        color = self.settings.get('color', 'gray')
        if color==None: kw = {}
        else: kw = {'class' : '%s_bg' % color }

        html = [
            formatter.rawHTML('<div class="shadowed_box">'),
            formatter.heading(1,3, **kw), title, formatter.heading(0,3),
            formatter.rawHTML('<div>'), content, formatter.rawHTML('</div>\n'* 2)
            ]

        self.request.write('\n'.join(html))


