# -*- coding: iso-8859-1 -*-
"""                             _vi
                             <Ii           _aa.
                             :I>          aZ2^
                              v`        .we^
        . .         ..        +        _2~
  ._auqXZZX   ._auqXZZZ`    ...._...   ~          ._|ii~
.aXZZY""^~~  vX#Z?""~~~._=ii|+++++++ii=,       _=|+~-
JXXX`       )XXX'    _|i+~   .__..._. +l=    -~-
SXXo        )XZX:   |i>  ._%i>~~+|ii| .i|   ._s_ass,,.    ._a%ssssssss
-SXZ6,,.    )XZX:  =l>  _li+~`   iii| .ii _uZZXX??YZ#Za, uXUX*?!!!!!!!
  "!XZ#ZZZZZXZXZ`  <i>  =i:     .|ii| .l|.dZXr      4XXo.XXXs,.
      -~^^^^^^^`   -||,  +i|=.   |ii| :i>:ZXZ(      ]XZX.-"SXZUZUXoa,,
                     +l|,  ~~|++|++i|||+~:ZXZ(      ]ZZX     ---~"?Z#m
            .__;=-     ~+l|=____.___,    :ZXZ(      ]ZXX_________auXX2
       ._||>+~-        .   -~+~++~~~-    :ZXZ(      ]ZXZZ#######UX*!"
       -+--          .>`      _
                   .<}`       3;
                 .<l>        .Zc
                .ii^         )Xo
                             ]XX

    MoinMoin - Inline javascript

    PURPOSE:
        Inliner du javascript

    AUTHOR:
        Antoine Durand-Gasselin <adg@crans.org>

    CALLING SEQUENCE:
       {{{
       #!javascript
       function prout () { return 1;}
       }}}
"""

Dependencies = []

#####################################################################
# Parser :  classe principale, c'est elle qui fait tout
#######

class Parser:

    def __init__(self, raw, request, **kw):
        self.form = request.form
        self.request = request
        self.raw = raw

    def format(self, formatter):
        # on utilise la classe qui va fabriquer le code html
        html = '\n<script type="text/javascript">\n<!--//\n' + self.raw +'\n//-->\n</script>\n\n'
        self.request.write(html)



