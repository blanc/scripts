#!/usr/bin/env python
# -*- coding: utf-8 -*-

from MoinMoin.wikiutil import escape

def execute(macro, text):
    return macro.formatter.rawHTML(
        '\n<script type="text/javascript" src="%s"></script>\n' % escape(text, 1))
