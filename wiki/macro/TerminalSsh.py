Dependencies = ["Time"]

class ssh:
    settings = {
        'protocol'         :   "ssh2",
        'server'           :   'ssh.crans.org',
        'port'             :   '22',
        "auth-method"      :   "keyboard-interactive",
        "bg-color"         :   "black",
        "fg-color"         :   "white",
        "cursor-color"     :   "yellow",
        "menus"            :   "pop3",
	"geometry"         :   "94x32",
        "allow-new-server" :   "false",
        }

    #appletLocation = "/wiki/applets/mindterm.jar"
    appletLocation = "https://ssh.crans.org/mindterm.jar"

    def parseArgs(self, args):
        argList = args.split(u',')

        for anArg in argList:
            try:
                key = anArg.split(u'=')[0]
                value = anArg.split(u'=')[1]
                self.settings[key] = value
            except:pass

    def formatParams(self):
        html = []
        for key, value in self.settings.items():
            html.append(u'<param name="%s" value="%s">' % (key, value))
        return u'\n'.join(html)

    def __init__(self, args):
        self.parseArgs(args)

    def run(self):
        html = [
            u'<p>',
            u'<applet code=com.mindbright.application.MindTerm.class width=667 height=481 archive="%s">' % self.appletLocation,
            self.formatParams(),
            u'</applet>',
            u'</p>',
        ]
        return u'\n'.join(html)

def execute(macro, args):
#    return macro.formatter.text("I got these args from a macro %s: %s" %
#    (str(macro), args))
    o = ssh(args)
    return macro.formatter.rawHTML(o.run())
