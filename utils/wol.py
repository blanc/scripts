#! /usr/bin/env python

import os, sys
sys.path.append('/usr/scripts/gestion')
from ldap_crans_test import crans_ldap

if len(sys.argv) > 1 : #Affichage de la syntaxe
    if (sys.argv[1] == '-h' or sys.argv[1] == '--help'):
        print 'Syntaxe:'
        print '  wol nom_de_la_machine'
        sys.exit(0)


login = os.getlogin()
db = crans_ldap()
try:
    aid = db.search('login=%s' % login)['adherent'][0]
except IndexError:
    print 'Adherent introuvable !'
    sys.exit(0)

if len(sys.argv) > 1 : #on a un nom de machine a reveiller
    nom = sys.argv[1]
    print 'Recherche de la machine %s' % nom
    try:
        mid = db.search('host=%s' % nom)['machine'][0]
    except IndexError:
        print 'Machine non trouvee!' #l'user est un manche et tente de reveiller les morts
        sys.exit(0)
    else:
        print 'Machine trouvee!' #on a maintenant une machine a reveiller
      
    if (mid not in aid.machines()) : #c'est sa machine ? Si oui, tout va bien, sinon...
        if 'Nounou' not in aid.droits() : #c'est une nounou? Si oui, tout va bien, sinon au revoir.
            print 'Vous n\'avez pas le droit de reveiller %s, ce n\'est pas votre machine' % nom
            sys.exit(0)

    #On a une machine, et l'user a le droit de la reveiller, donc on lance le paquet magique
    etherwake = os.popen("/usr/bin/sudo /usr/sbin/etherwake -i crans %s" % mid.mac())
    print 'Paquet magique envoye a %s' % nom

else: #On n'a pas de nom de machine, on propose les machines de l'adherent
    print 'Quelle machine voulez vous reveiller ?'
    for machines in aid.machines() :
        print ' - %s' % machines.nom()
    print 'Syntaxe:'
    print '  wol nom_de_la_machine'


