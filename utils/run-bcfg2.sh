#!/bin/bash

# run-bcfg2.sh
# ------------
# Copyright : (c) 2008, Jeremie Dimino <jeremie@dimino.org>
# Licence   : BSD3

# Script pour lancer bcfg2 -v -I sur tous les serveurs.
#
# Pour l'utiliser vous devez le copier sur votre ordi, et le lancer
# avec en argument une liste de serveurs:
#
# $ ./run-bcfg2.sh rouge zamok
#
# Sans aucun serveur il récupère la liste de tous les serveurs depuis
# la conf de bcfg2.

# Pour que ça marche il faut que vous puissiez vous connecter aux
# serveurs sans mot de passe et que vous aiyez déjà accepté la clef
# pour chaque serveur

# Liste des machines à contacter
if [ $# = 0 ]; then
    echo "Récupération de la liste des machines depuis la conf de bcfg2..."
    HOSTS=$(ssh vert.adm.crans.org grep -o "'name=\"[^\"]*\"'" /var/lib/bcfg2/Metadata/clients.xml | cut -d'"' -f2)
else
    HOSTS="$@"
fi

echo "Veuillez taper votre mot de passe du crans:"
read -s -p "password: " password
echo

echo "Obtention des tokens pour sudo..."
pids=""
for host in $HOSTS; do
    echo $password | ssh $host sudo -S true & pids="$pids $!"
done

# On attend que tous les sudo soient terminés
for pid in $pids; do
    wait $pid
done

echo "Lancement de bcfg2 sur tous les serveurs"
for host in $HOSTS; do
    echo "  $host"
    x-terminal-emulator -title $host -e ssh -t $host sudo bcfg2 -v -I &
done
