#!/bin/sh

# Script appelé par cron pour mettre à jour le fichier vendeur
# Avant la comande était entièrement dans la crontab, dans un script ça avait l'air mieux...
# 
# Le cron est désormais appelé sur charybde (afin de permettre un push facile)
# avec le chemin vers le dépôt et la branche à pusher (master).
# Un lancement sans argument est toutefois accepté.


TEMPFILE=`tempfile`

# on évite de casser le dépôt (umask)
umask 002
wget -o /dev/null -O $TEMPFILE http://standards.ieee.org/regauth/oui/oui.txt \
 && awk -F '[\t ]*(hex)[ \t]*' '(/(hex)/) {gsub("-",":",$1) ; {gsub("^ *", "", $1)}; {gsub("\\(", "", $1)}; {gsub("\\)", "", $2)}; print $1" "$2}' < $TEMPFILE > /usr/scripts/gestion/ethercodes.dat \
 && cd /usr/scripts/gestion  \
 && git commit --author="Cron Daemon <root@crans.org>" -m "[ethercodes.dat] Mise à jour du fichier vendeur" ethercodes.dat > /dev/null \
 && git push "$@" > /dev/null

rm -f $TEMPFILE
